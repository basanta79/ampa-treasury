#!/bin/sh
set -e

# Variables de entorno
export PGPASSWORD="$POSTGRES_PASSWORD"

# Crear una nueva base de datos
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE DATABASE treasury;
    GRANT ALL PRIVILEGES ON DATABASE treasury TO $POSTGRES_USER;
EOSQL