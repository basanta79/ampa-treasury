package com.sodges.ampa.application.service;

import com.sodges.ampa.domain.model.Project;
import com.sodges.ampa.domain.repository.ProjectRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProjectServiceTest {

    @Mock // Used to create and inject mocked instances
    private ProjectRepository projectRepository;

    @InjectMocks // Create an instance of the class to be tested and inject mock on it
    private ProjectService projectService;

    @Test
    public void testGetProjectById() {
        UUID projectId = UUID.randomUUID();

        Project project = new Project(projectId, "PRJ123", "Project Description", "23-24");

        // Use when to specify the behaviour of the specified mocked repository
        when(projectRepository.findById(projectId)).thenReturn(Optional.of(project));

        Optional<Project> foundProject = projectService.getProjectById(projectId);

        assertTrue(foundProject.isPresent());
        assertEquals(projectId, foundProject.get().getId());

        // Used to check if certain method is called.
        verify(projectRepository, times(1)).findById(projectId);
    }

}
