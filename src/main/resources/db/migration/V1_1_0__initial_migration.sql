CREATE TABLE IF NOT EXISTS balance_accounts
(
    id uuid NOT NULL PRIMARY KEY,
    description VARCHAR(256) NOT NULL,
    balance NUMERIC(15,2) NOT NULL,
    currency VARCHAR(5) NOT NULL
);

CREATE TABLE IF NOT EXISTS ledger_records
(
    id uuid NOT NULL PRIMARY KEY,
    description TEXT NOT NULL,
    category TEXT NOT NULL,
    amount NUMERIC(15,2) NOT NULL,
    currency TEXT NOT NULL,
    operation_date TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS periods
(
    id uuid NOT NULL PRIMARY KEY,
    period TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS projects
(
    id uuid NOT NULL PRIMARY KEY,
    project_key TEXT NOT NULL,
    description TEXT NOT NULL,
    period TEXT NOT NULL
);