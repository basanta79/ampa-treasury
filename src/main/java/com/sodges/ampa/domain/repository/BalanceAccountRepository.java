package com.sodges.ampa.domain.repository;

import com.sodges.ampa.domain.model.BalanceAccount;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BalanceAccountRepository {
    public BalanceAccount save(BalanceAccount balanceAccount);
    public Optional<BalanceAccount> findById(UUID id);
    public List<BalanceAccount> getAll();
}
