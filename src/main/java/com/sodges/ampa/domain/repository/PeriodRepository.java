package com.sodges.ampa.domain.repository;

import com.sodges.ampa.domain.model.Period;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PeriodRepository {
    Optional<Period> findById(UUID id);
    Period save(Period period);
    List<Period> getAll();
}
