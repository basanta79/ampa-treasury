package com.sodges.ampa.domain.repository;

import com.sodges.ampa.domain.model.LedgerRecord;

import java.util.List;
import java.util.UUID;

public interface LedgerRecordRepository {
    public LedgerRecord save(LedgerRecord ledgerRecord);
    List<LedgerRecord> getAllFromBalance(UUID balanceAccountId);
}