package com.sodges.ampa.domain.repository;

import com.sodges.ampa.domain.model.Project;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository {
    Optional<Project> findById(UUID id);
    Project save(Project project);
    List<Project> getAll();
}
