package com.sodges.ampa.domain.model;

import lombok.*;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LedgerRecord {
    private UUID id;
    private String description;
    private String category;
    private BigDecimal amount;
    private String currency;
    private OffsetDateTime operationDate;
}
