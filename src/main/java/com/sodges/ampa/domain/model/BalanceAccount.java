package com.sodges.ampa.domain.model;

import lombok.*;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BalanceAccount {
    private UUID id;
    private String description;
    private BigDecimal balance;
    private Currency currency;

}
