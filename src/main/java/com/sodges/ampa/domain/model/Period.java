package com.sodges.ampa.domain.model;

import lombok.*;

import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Period {
    private UUID id;
    private String period;
}
