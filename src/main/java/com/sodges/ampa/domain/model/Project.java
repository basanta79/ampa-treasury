package com.sodges.ampa.domain.model;

import lombok.*;

import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Project {
    private UUID id;
    private String projectKey;
    private String description;
    private String period;
}
