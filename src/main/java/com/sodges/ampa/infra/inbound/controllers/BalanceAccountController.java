package com.sodges.ampa.infra.inbound.controllers;

import com.sodges.ampa.application.service.BalanceAccountService;
import com.sodges.ampa.domain.model.BalanceAccount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "balance-account")
public class BalanceAccountController {

    private final BalanceAccountService balanceAccountService;

    @Autowired
    public BalanceAccountController(BalanceAccountService balanceAccountService) {
        this.balanceAccountService = balanceAccountService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public BalanceAccount create(@RequestBody BalanceAccount balanceAccount) {
        return balanceAccountService.create(balanceAccount);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<BalanceAccount> get(@PathVariable UUID id){
        return balanceAccountService.findById(id);
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BalanceAccount> getAll(){
        return balanceAccountService.getAll();
    }
}
