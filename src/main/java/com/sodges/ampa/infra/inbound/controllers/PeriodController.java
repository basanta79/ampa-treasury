package com.sodges.ampa.infra.inbound.controllers;

import com.sodges.ampa.application.service.PeriodService;
import com.sodges.ampa.domain.model.Period;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "period")
public class PeriodController {

    private final PeriodService periodService;

    @Autowired
    public PeriodController(PeriodService periodService) {
        this.periodService = periodService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Period create(@RequestBody Period period) {
        return periodService.create(period);
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Period> getAll(){
        return periodService.getAll();
    }
}
