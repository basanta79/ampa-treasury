package com.sodges.ampa.infra.inbound.controllers;

import com.sodges.ampa.application.service.LedgerRecordService;
import com.sodges.ampa.domain.model.LedgerRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "ledger-record")
public class LedgerRecordController {

    private final LedgerRecordService ledgerRecordService;

    @Autowired
    public LedgerRecordController(LedgerRecordService ledgerRecordService) {
        this.ledgerRecordService = ledgerRecordService;
    }

    @PostMapping(value = "/credit", produces = MediaType.APPLICATION_JSON_VALUE)
    public LedgerRecord credit(@RequestBody LedgerRecord ledgerRecord) {
        return ledgerRecordService.credit(ledgerRecord);
    }

    @PostMapping(value = "/debit", produces = MediaType.APPLICATION_JSON_VALUE)
    public LedgerRecord debit(@RequestBody LedgerRecord ledgerRecord) {
        return ledgerRecordService.debit(ledgerRecord);
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LedgerRecord> get(@RequestParam UUID balanceAccountId){
        return ledgerRecordService.getAllFromBalance(balanceAccountId);
    }

}
