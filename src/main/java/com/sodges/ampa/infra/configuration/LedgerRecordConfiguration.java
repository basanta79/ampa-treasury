package com.sodges.ampa.infra.configuration;

import com.sodges.ampa.domain.repository.LedgerRecordRepository;
import com.sodges.ampa.infra.persistance.adapter.JpaLedgerRecordRepositoryAdapter;
import com.sodges.ampa.infra.persistance.repository.JpaLedgerRecordRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LedgerRecordConfiguration {

    @Bean
    public LedgerRecordRepository ledgerRecordRepository(JpaLedgerRecordRepository jpaLedgerRecordRepository) {
        return new JpaLedgerRecordRepositoryAdapter(jpaLedgerRecordRepository);
    }
}
