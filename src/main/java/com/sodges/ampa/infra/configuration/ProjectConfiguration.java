package com.sodges.ampa.infra.configuration;

import com.sodges.ampa.domain.repository.ProjectRepository;
import com.sodges.ampa.infra.persistance.adapter.JpaProjectRepositoryAdapter;
import com.sodges.ampa.infra.persistance.repository.JpaProjectRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProjectConfiguration {

    @Bean
    public ProjectRepository projectRepository(JpaProjectRepository jpaProjectRepository) {
        return new JpaProjectRepositoryAdapter(jpaProjectRepository);
    }
}
