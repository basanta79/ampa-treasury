package com.sodges.ampa.infra.configuration;

import com.sodges.ampa.domain.repository.BalanceAccountRepository;
import com.sodges.ampa.infra.persistance.adapter.JpaBalanceAccountRepositoryAdapter;
import com.sodges.ampa.infra.persistance.repository.JpaBalanceAccountRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BalanceAccountConfiguration {

    @Bean
    public BalanceAccountRepository balanceAccountRepository(JpaBalanceAccountRepository jpaBalanceAccountRepository) {
        return new JpaBalanceAccountRepositoryAdapter(jpaBalanceAccountRepository);
    }
}
