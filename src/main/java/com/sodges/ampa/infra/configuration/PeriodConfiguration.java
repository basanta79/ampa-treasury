package com.sodges.ampa.infra.configuration;

import com.sodges.ampa.domain.repository.PeriodRepository;
import com.sodges.ampa.infra.persistance.adapter.JpaPeriodRepositoryAdapter;
import com.sodges.ampa.infra.persistance.repository.JpaPeriodRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PeriodConfiguration {

    @Bean
    public PeriodRepository periodRepository(JpaPeriodRepository jpaPeriodRepository) {
        return new JpaPeriodRepositoryAdapter(jpaPeriodRepository);
    }
}
