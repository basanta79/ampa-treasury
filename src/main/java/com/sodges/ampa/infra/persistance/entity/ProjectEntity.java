package com.sodges.ampa.infra.persistance.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "projects")
public class ProjectEntity {

    @Id
    private UUID id;

    @Column(name = "project_key", length = 25, nullable = false)
    private String projectKey;

    @Column(name = "description")
    private String description;

    @Column(name = "period")
    private String period;
}