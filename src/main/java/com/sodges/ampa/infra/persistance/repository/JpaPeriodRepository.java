package com.sodges.ampa.infra.persistance.repository;

import com.sodges.ampa.infra.persistance.entity.PeriodEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JpaPeriodRepository extends JpaRepository<PeriodEntity, UUID> {
}
