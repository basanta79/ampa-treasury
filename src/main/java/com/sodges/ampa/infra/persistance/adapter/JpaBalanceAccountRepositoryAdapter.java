package com.sodges.ampa.infra.persistance.adapter;

import com.sodges.ampa.domain.model.BalanceAccount;
import com.sodges.ampa.domain.repository.BalanceAccountRepository;
import com.sodges.ampa.infra.persistance.entity.BalanceAccountEntity;
import com.sodges.ampa.infra.persistance.repository.JpaBalanceAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class JpaBalanceAccountRepositoryAdapter implements BalanceAccountRepository {
    private final JpaBalanceAccountRepository jpaBalanceAccountRepository;

    @Autowired
    public JpaBalanceAccountRepositoryAdapter(JpaBalanceAccountRepository jpaBalanceAccountRepository) {
        this.jpaBalanceAccountRepository = jpaBalanceAccountRepository;
    }

    @Override
    public BalanceAccount save(BalanceAccount balanceAccount) {
        return toDomain(jpaBalanceAccountRepository.save(toEntity(balanceAccount)));
    };

    @Override
    public Optional<BalanceAccount> findById(UUID id) {
        return jpaBalanceAccountRepository.findById(id).map(this::toDomain);
    };

    @Override
    public List<BalanceAccount> getAll() {
        List<BalanceAccountEntity> entityList = jpaBalanceAccountRepository.findAll();
        List<BalanceAccount> modelList = new ArrayList<>();
        for (BalanceAccountEntity entity : entityList) {
            modelList.add(toDomain(entity));
        }
        return modelList;
    };


    private BalanceAccountEntity toEntity(BalanceAccount balanceAccount) {
        BalanceAccountEntity entity = new BalanceAccountEntity();
        entity.setId(balanceAccount.getId());
        entity.setDescription(balanceAccount.getDescription());
        entity.setBalance(balanceAccount.getBalance());
        entity.setCurrency(balanceAccount.getCurrency());
        return entity;
    }

    private BalanceAccount toDomain(BalanceAccountEntity entity) {
        BalanceAccount balanceAccount = new BalanceAccount();
        balanceAccount.setId(entity.getId());
        balanceAccount.setDescription(entity.getDescription());
        balanceAccount.setBalance(entity.getBalance());
        balanceAccount.setCurrency(entity.getCurrency());
        return balanceAccount;
    }
}
