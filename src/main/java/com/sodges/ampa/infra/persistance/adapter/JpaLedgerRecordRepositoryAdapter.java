package com.sodges.ampa.infra.persistance.adapter;

import com.sodges.ampa.domain.model.LedgerRecord;
import com.sodges.ampa.domain.repository.LedgerRecordRepository;
import com.sodges.ampa.infra.persistance.entity.LedgerRecordEntity;
import com.sodges.ampa.infra.persistance.repository.JpaLedgerRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class JpaLedgerRecordRepositoryAdapter implements LedgerRecordRepository {
    private final JpaLedgerRecordRepository jpaLedgerRecordRepository;

    @Autowired
    public JpaLedgerRecordRepositoryAdapter(JpaLedgerRecordRepository jpaLedgerRecordRepository) {
        this.jpaLedgerRecordRepository = jpaLedgerRecordRepository;
    }

    @Override
    public LedgerRecord save(LedgerRecord ledgerRecord) {
        return toDomain(jpaLedgerRecordRepository.save(toEntity(ledgerRecord)));
    };

    @Override
    public List<LedgerRecord> getAllFromBalance(UUID balanceAccountId) {
        List<LedgerRecordEntity> entityList = jpaLedgerRecordRepository.findAll();
        List<LedgerRecord> modelList = new ArrayList<>();
        for (LedgerRecordEntity entity : entityList) {
            modelList.add(toDomain(entity));
        }
        return modelList;
    };

    private LedgerRecordEntity toEntity(LedgerRecord ledgerRecord) {
        LedgerRecordEntity entity = new LedgerRecordEntity();
        entity.setId(ledgerRecord.getId());
        entity.setDescription(ledgerRecord.getDescription());
        entity.setCategory(ledgerRecord.getCategory());
        entity.setAmount(ledgerRecord.getAmount());
        entity.setCurrency(ledgerRecord.getCurrency());
        entity.setOperationDate(ledgerRecord.getOperationDate());
        return entity;
    }

    private LedgerRecord toDomain(LedgerRecordEntity entity) {
        LedgerRecord ledgerRecord = new LedgerRecord();
        ledgerRecord.setId(entity.getId());
        ledgerRecord.setDescription(entity.getDescription());
        ledgerRecord.setCategory(entity.getCategory());
        ledgerRecord.setAmount(entity.getAmount());
        ledgerRecord.setCurrency(entity.getCurrency());
        ledgerRecord.setOperationDate(entity.getOperationDate());
        return ledgerRecord;
    }

}
