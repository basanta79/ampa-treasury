package com.sodges.ampa.infra.persistance.repository;

import com.sodges.ampa.infra.persistance.entity.ProjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JpaProjectRepository extends JpaRepository<ProjectEntity, UUID> {
    // Additional query methods if necessary
}
