package com.sodges.ampa.infra.persistance.repository;

import com.sodges.ampa.infra.persistance.entity.LedgerRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JpaLedgerRecordRepository extends JpaRepository<LedgerRecordEntity, UUID> {
}
