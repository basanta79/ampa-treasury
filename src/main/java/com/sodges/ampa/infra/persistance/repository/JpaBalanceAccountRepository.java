package com.sodges.ampa.infra.persistance.repository;

import com.sodges.ampa.infra.persistance.entity.BalanceAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JpaBalanceAccountRepository extends JpaRepository<BalanceAccountEntity, UUID> {
}
