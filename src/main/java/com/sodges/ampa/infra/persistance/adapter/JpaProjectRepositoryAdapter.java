package com.sodges.ampa.infra.persistance.adapter;

import com.sodges.ampa.domain.model.Project;
import com.sodges.ampa.domain.repository.ProjectRepository;
import com.sodges.ampa.infra.persistance.entity.ProjectEntity;
import com.sodges.ampa.infra.persistance.repository.JpaProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class JpaProjectRepositoryAdapter implements ProjectRepository {
    private final JpaProjectRepository jpaProjectRepository;

    @Autowired
    public JpaProjectRepositoryAdapter(JpaProjectRepository jpaProjectRepository) {
        this.jpaProjectRepository = jpaProjectRepository;
    }

    @Override
    public Optional<Project> findById(UUID id) {
        return jpaProjectRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Project> getAll() {
        List<ProjectEntity> list = jpaProjectRepository.findAll();
        List<Project> returnList = new ArrayList<Project>();
        for (ProjectEntity entity: list) {
            returnList.add(toDomain(entity));
        }
        return returnList;
    }

    @Override
    public Project save(Project project) {
        ProjectEntity projectEntity = toEntity(project);
        return toDomain(jpaProjectRepository.save(projectEntity));
    }

    private ProjectEntity toEntity(Project project) {
        ProjectEntity entity = new ProjectEntity();
        entity.setId(project.getId());
        entity.setProjectKey(project.getProjectKey());
        entity.setDescription(project.getDescription());
        entity.setPeriod(project.getPeriod());
        return entity;
    }

    private Project toDomain(ProjectEntity entity) {
        Project project = new Project();
        project.setId(entity.getId());
        project.setProjectKey(entity.getProjectKey());
        project.setDescription(entity.getDescription());
        project.setPeriod(entity.getPeriod());
        return project;
    }
}
