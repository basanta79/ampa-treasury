package com.sodges.ampa.infra.persistance.adapter;

import com.sodges.ampa.domain.model.Period;
import com.sodges.ampa.domain.repository.PeriodRepository;
import com.sodges.ampa.infra.persistance.entity.PeriodEntity;
import com.sodges.ampa.infra.persistance.repository.JpaPeriodRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class JpaPeriodRepositoryAdapter implements PeriodRepository {
    private final JpaPeriodRepository jpaPeriodRepository;

    @Autowired
    public JpaPeriodRepositoryAdapter(JpaPeriodRepository jpaPeriodRepository) {
        this.jpaPeriodRepository = jpaPeriodRepository;
    }

    @Override
    public Optional<Period> findById(UUID id) {
        return jpaPeriodRepository.findById(id).map(this::toDomain);
    };

    @Override
    public Period save(Period period) {
        PeriodEntity entity = toEntity(period);
        return toDomain(jpaPeriodRepository.save(entity));
    };

    @Override
    public List<Period> getAll() {
        List<PeriodEntity> entityList = jpaPeriodRepository.findAll();
        List<Period> modelList = new ArrayList<Period>();
        for (PeriodEntity periodEntity: entityList) {
            modelList.add(toDomain(periodEntity));
        }
        return modelList;
    };

    private PeriodEntity toEntity(Period period) {
        PeriodEntity entity = new PeriodEntity();
        entity.setId(period.getId());
        entity.setPeriod(period.getPeriod());
        return entity;
    }

    private Period toDomain(PeriodEntity entity) {
        Period period = new Period();
        period.setId(entity.getId());
        period.setPeriod(entity.getPeriod());
        return period;
    }

}
