package com.sodges.ampa.infra.persistance.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "balance_accounts")
public class BalanceAccountEntity {

    @Id
    private UUID id;

    @Column(name = "description")
    private String description;

    @Column(name = "balance")
    private BigDecimal balance;

    @Column(name = "currency")
    private Currency currency;
}
