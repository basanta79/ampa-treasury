package com.sodges.ampa.infra.persistance.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "periods")
public class PeriodEntity {
    @Id
    private UUID id;

    @Column(name = "period")
    private String period;
}
