package com.sodges.ampa.application.service;

import com.sodges.ampa.domain.model.LedgerRecord;
import com.sodges.ampa.domain.repository.LedgerRecordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class LedgerRecordService {

    private final LedgerRecordRepository ledgerRecordRepository;

    public LedgerRecord credit(LedgerRecord ledgerRecord) {
        ledgerRecord.setId(UUID.randomUUID());
        ledgerRecord.setCategory("CREDIT");
        ledgerRecord.setOperationDate(OffsetDateTime.now());
        return ledgerRecordRepository.save(ledgerRecord);
    }

    public LedgerRecord debit(LedgerRecord ledgerRecord) {
        ledgerRecord.setId(UUID.randomUUID());
        ledgerRecord.setCategory("DEBIT");
        ledgerRecord.setOperationDate(OffsetDateTime.now());
        return ledgerRecordRepository.save(ledgerRecord);
    }

    public List<LedgerRecord> getAllFromBalance(UUID balanceAccountId) {
        return ledgerRecordRepository.getAllFromBalance(balanceAccountId);
    }
}
