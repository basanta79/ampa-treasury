package com.sodges.ampa.application.service;

import com.sodges.ampa.domain.model.Period;
import com.sodges.ampa.domain.repository.PeriodRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PeriodService {
    private final PeriodRepository periodRepository;

    public Period create(Period period) {
        period.setId(UUID.randomUUID());
        return periodRepository.save(period);
    }

    public List<Period> getAll() {
        return periodRepository.getAll();
    }

}
