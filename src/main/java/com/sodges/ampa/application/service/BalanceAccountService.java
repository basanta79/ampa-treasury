package com.sodges.ampa.application.service;

import com.sodges.ampa.domain.model.BalanceAccount;
import com.sodges.ampa.domain.repository.BalanceAccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BalanceAccountService {
    private final BalanceAccountRepository balanceAccountRepository;

    public BalanceAccount create(BalanceAccount balanceAccount) {
        balanceAccount.setId(UUID.randomUUID());
        balanceAccount.setBalance(BigDecimal.valueOf(0.0));
        return balanceAccountRepository.save(balanceAccount);
    }

    public Optional<BalanceAccount> findById(UUID id) {
        return balanceAccountRepository.findById(id);
    }

    public List<BalanceAccount> getAll() {
        return balanceAccountRepository.getAll();
    }
}
