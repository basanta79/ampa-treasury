package com.sodges.ampa.application.service;

import com.sodges.ampa.domain.model.Project;
import com.sodges.ampa.domain.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectRepository projectRepository;

    public Optional<Project> getProjectById(UUID id) {
        return projectRepository.findById(id);
    }

    public Project create(Project project) {
        project.setId(UUID.randomUUID());
        return projectRepository.save(project);
    }

    public List<Project> getAllProjects() {
        return projectRepository.getAll();
    }

}
